# Project 01: Idlebin

This is [Project 01] of [CSE-30341-FA18].

## Members

- Domer McDomerson (dmcdomer@nd.edu)
- Belle Fleur (bfleur@nd.edu)

## Demonstration

[Link to Demonstration Slides]()

## Applets

### Group

- [ ] `ls [-Ra] [PATHS]...` (**6**)

- [ ] `cp [-Rv] SOURCE... TARGET` (**6**)

### Individual

- [ ] `chmod OCTAL-MODE FILE...` (**3**)

- [ ] `du -bc [FILE]...` (**6**)

- [ ] `ln [-sv] TARGET LINK_NAME` (**3**)

- [ ] `mkdir [-v] DIRECTORY...` (**3**)

- [ ] `mv [-v] SOURCE ... DESTINATION` (**6**)

- [ ] `realpath [-e] FILE...` (**3**)

- [ ] `rm [-Rv]` (**6**)

- [ ] `rmdir [-v] DIRECTORY...` (**3**)

- [ ] `touch [FILES]...` (**3**)

- [ ] `uname [-asnrvmo]` (**3**)

[Project 01]:       https://www3.nd.edu/~pbui/teaching/cse.30341.fa18/project01.html
[CSE-30341-FA18]:   https://www3.nd.edu/~pbui/teaching/cse.30341.fa18/
