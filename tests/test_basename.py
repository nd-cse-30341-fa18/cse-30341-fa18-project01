#!/usr/bin/env python3

import os
import subprocess
import sys
import unittest

from subprocess import PIPE, STDOUT

# Basename Test Case

class BasenameTestCase(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        print('\nTesting basename...', file=sys.stderr)

    def check_output(self, arguments, expected):
        command = 'bin/idlebin basename {}'.format(arguments)
        process = subprocess.run(command, stdout=PIPE, stderr=STDOUT, shell=True)
        output  = process.stdout.decode('utf-8').rstrip()

        self.assertEqual(output, expected)
        self.assertEqual(process.returncode, 0)

    def check_valgrind(self, arguments):
        command = 'valgrind --leak-check=full bin/idlebin basename {}'.format(arguments)
        process = subprocess.run(command, stdout=PIPE, stderr=STDOUT, shell=True)
        lines   = process.stdout.split(b'\n')
        errors  = [int(l.split()[3]) for l in lines if b'ERROR SUMMARY' in l]

        self.assertEqual(process.returncode, 0)
        self.assertEqual(errors, [0])

    def test_00_help(self):
        command = 'bin/idlebin basename --help'.split()
        process = subprocess.run(command, stdout=PIPE, stderr=STDOUT)
        self.assertEqual(process.stdout, b'Usage: basename NAME [SUFFIX]\n')

    def test_01_usage(self):
        command = 'bin/idlebin basename'.split()
        process = subprocess.run(command, stdout=PIPE, stderr=STDOUT)
        self.assertEqual(process.stdout, b'Usage: basename NAME [SUFFIX]\n')

    def test_02_basename(self):
        self.check_output('/foo/ls', 'ls')

    def test_02_basename_valgrind(self):
        self.check_valgrind('/foo/ls')

    def test_03_basename_suffix(self):
        self.check_output('/foo/ls.c .c', 'ls')

    def test_03_basename_suffix_valgrind(self):
        self.check_valgrind('/foo/ls.c .c')

    def test_04_basename_bad_suffix(self):
        self.check_output('/foo/ls.c .cpp', 'ls.c')

    def test_04_basename_bad_suffix_valgrind(self):
        self.check_valgrind('/foo/ls.c .cpp')

    def test_05_basename_name_suffix(self):
        self.check_output('/foo/ls.c ls.c', 'ls.c')

    def test_05_basename_name_suffix_valgrind(self):
        self.check_valgrind('/foo/ls.c ls.c')

# Main Execution

if __name__ == '__main__':
    unittest.main()

# vim: set sts=4 sw=4 ts=8 expandtab ft=python:
