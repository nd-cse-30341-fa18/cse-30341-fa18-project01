#!/usr/bin/env python3

import os
import subprocess
import sys
import unittest

from subprocess import PIPE, STDOUT

# Ls Test Case

class LsTestCase(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        print('\nTesting ls...', file=sys.stderr)
        os.environ['LC_ALL'] = 'C'

    def check_output(self, arguments, expect_error=False):
        src_cmd = 'bin/idlebin ls {}'.format(arguments)
        src_prc = subprocess.run(src_cmd, stdout=PIPE, stderr=STDOUT, shell=True)
        src_out = src_prc.stdout

        tgt_cmd = '/bin/ls {}'.format(arguments)
        tgt_prc = subprocess.run(tgt_cmd, stdout=PIPE, stderr=STDOUT, shell=True)
        tgt_out = tgt_prc.stdout

        if expect_error:
            src_out = src_out.split(b':', 1)[-1].replace(b"'", b"")
            tgt_out = tgt_out.split(b':', 1)[-1].replace(b"'", b"")

        self.assertEqual(src_out, tgt_out)

        if expect_error:
            self.assertNotEqual(src_prc.returncode, 0)
            self.assertNotEqual(tgt_prc.returncode, 0)
        else:
            self.assertEqual(src_prc.returncode, 0)
            self.assertEqual(tgt_prc.returncode, 0)

    def check_valgrind(self, arguments, expect_error=False):
        command = 'valgrind --leak-check=full bin/idlebin ls {}'.format(arguments)
        process = subprocess.run(command, stdout=PIPE, stderr=STDOUT, shell=True)
        lines   = process.stdout.split(b'\n')
        errors  = [int(l.split()[3]) for l in lines if b'ERROR SUMMARY' in l]

        self.assertEqual(errors, [0])
        if expect_error:
            self.assertNotEqual(process.returncode, 0)
        else:
            self.assertEqual(process.returncode, 0)

    def test_00_help(self):
        command = 'bin/idlebin ls --help'.split()
        process = subprocess.run(command, stdout=PIPE, stderr=STDOUT)
        self.assertEqual(process.stdout, b'Usage: ls [-Ra] [PATHS]...\n'
            b'   -R    List subdirectories recursively\n'
            b'   -a    Do not ignore entries starting with .\n'
        )

    def test_01_implicit_curdir(self):
        self.check_output('')

    def test_01_implicit_curdir_valgrind(self):
        self.check_valgrind('')

    def test_02_explicit_curdir(self):
        self.check_output('.')

    def test_02_explicit_curdir_valgrind(self):
        self.check_valgrind('.')

    def test_03_showhidden_curdir(self):
        self.check_output('-a')

    def test_03_showhidden_curdir_valgrind(self):
        self.check_valgrind('-a')

    def test_04_multiple(self):
        self.check_output('*')

    def test_04_multiple_valgrind(self):
        self.check_valgrind('*')

    def test_05_showhidden_multiple(self):
        self.check_output('-a *')

    def test_05_showhidden_multiple_valgrind(self):
        self.check_valgrind('-a *')

    def test_06_recursive_curdir(self):
        self.check_output('-R')

    def test_06_recursive_curdir_valgrind(self):
        self.check_valgrind('-R')

    def test_07_recursive_showhidden_curdir(self):
        self.check_output('-Ra')

    def test_07_recursive_curdir_showhidden_valgrind(self):
        self.check_valgrind('-Ra')

    def test_08_dne(self):
        self.check_output('/DNE', True)

    def test_08_dne_valgrind(self):
        self.check_valgrind('/DNE', True)

# Main Execution

if __name__ == '__main__':
    unittest.main()

# vim: set sts=4 sw=4 ts=8 expandtab ft=python:
