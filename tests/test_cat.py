#!/usr/bin/env python3

import os
import subprocess
import sys
import unittest

from subprocess import PIPE, STDOUT

# Cat Test Case

class CatTestCase(unittest.TestCase):

    FILES = ('README.md', 'Makefile')

    @classmethod
    def setUpClass(cls):
        print('\nTesting cat...', file=sys.stderr)

    def check_output(self, arguments, expected, returncode=0):
        command = 'bin/idlebin cat {}'.format(arguments)
        process = subprocess.run(command, stdout=PIPE, stderr=STDOUT, shell=True)
        output  = process.stdout.decode('utf-8')

        self.assertEqual(output, expected)
        self.assertEqual(process.returncode, returncode)

    def check_valgrind(self, arguments, returncode=0):
        command = 'valgrind --leak-check=full bin/idlebin cat {}'.format(arguments)
        process = subprocess.run(command, stdout=PIPE, stderr=STDOUT, shell=True)
        lines   = process.stdout.split(b'\n')
        errors  = [int(l.split()[3]) for l in lines if b'ERROR SUMMARY' in l]

        self.assertEqual(process.returncode, returncode)
        self.assertEqual(errors, [0])

    def test_00_help(self):
        command = 'bin/idlebin cat --help'.split()
        process = subprocess.run(command, stdout=PIPE, stderr=STDOUT)
        self.assertEqual(process.stdout,
            b'Usage: cat [-E] [FILES]...\n'
            b'   -E    Display $ at end of each line\n'
        )

    def test_01_implicit_stdin(self):
        with open('README.md') as fs:
            self.check_output('< README.md', fs.read())

    def test_01_implicit_stdin_valgrind(self):
        self.check_valgrind('< README.md')

    def test_02_explicit_stdin(self):
        with open('README.md') as fs:
            self.check_output('README.md', fs.read())

    def test_02_explicit_stdin_valgrind(self):
        self.check_valgrind('README.md')

    def test_03_multiple_files(self):
        streams = [open(f) for f in self.FILES]
        self.check_output(' '.join(self.FILES), ''.join(s.read() for s in streams))
        streams = [s.close() for s in streams]

    def test_03_multiple_files_valgrind(self):
        self.check_valgrind(' '.join(self.FILES))

    def test_04_implicit_stdin_endings(self):
        stream = open('README.md')
        self.check_output('-E < README.md', ''.join(
            l[:-1] + '$\n' for l in stream
        ))
        stream.close()

    def test_04_implicit_stdin_endings_valgrind(self):
        self.check_valgrind('-E < README.md')

    def test_05_DNE(self):
        self.check_output('/DNE', '/DNE: No such file or directory\n', 1)

    def test_05_DNE_valgrind(self):
        self.check_valgrind('/DNE', 1)

# Main Execution

if __name__ == '__main__':
    unittest.main()

# vim: set sts=4 sw=4 ts=8 expandtab ft=python:
