#!/usr/bin/env python3

import os
import subprocess
import sys
import unittest

from subprocess import PIPE, STDOUT

# False Test Case

class FalseTestCase(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        print('\nTesting false...', file=sys.stderr)

    def test_00_help(self):
        command = 'bin/idlebin false --help'.split()
        process = subprocess.run(command, stdout=PIPE, stderr=STDOUT)
        self.assertEqual(process.stdout, b'Usage: false\n')

    def test_01_status(self):
        command = 'bin/idlebin false'.split()
        process = subprocess.run(command)
        self.assertNotEqual(process.returncode, 0)

    def test_02_valgrind(self):
        command = 'valgrind --leak-check=full bin/idlebin false'.split()
        process = subprocess.run(command, stdout=PIPE, stderr=STDOUT)
        lines   = process.stdout.split(b'\n')
        errors  = [int(l.split()[3]) for l in lines if b'ERROR SUMMARY' in l]

        self.assertNotEqual(process.returncode, 0)
        self.assertEqual(errors, [0])

# Main Execution

if __name__ == '__main__':
    unittest.main()

# vim: set sts=4 sw=4 ts=8 expandtab ft=python:
