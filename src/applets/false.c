/* false.c: false applet */

#include "idlebin.h"

/* Usage */

const char FALSE_USAGE[] = "Usage: false"; 

/* Applet */

int false_applet(int argc, char *argv[]) {
    return EXIT_FAILURE;
}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
